import { TestBed } from '@angular/core/testing';
import { UtilsService } from './utils.service';

describe('UtilsService', () => {
	let service: UtilsService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
        service = TestBed.inject(UtilsService);
	});

	it('should be created', () => {
		expect(service).toBeTruthy();
	});
	describe('replaceUrlParams ', () => {
		it('try it with not valid parameters', () => {
			expect(service.replaceUrlParams(null, null)).toEqual('');
        });
        it('try it with valid parameters', () => {
            let maskReplaceSingleSpy = spyOn(service, '_maskReplaceSingle').and.returnValue('http://www.test.com/1');
            service.replaceUrlParams('http://www.test.com/[[ct]]', {ct: '1'});
            expect(maskReplaceSingleSpy).toHaveBeenCalledWith('http://www.test.com/[[ct]]', 'ct', '1');
        });
    });
    describe('_maskReplaceSingle ', () => {
		it('try it with not valid parameters', () => {
			expect(service._maskReplaceSingle(null, null, null)).toEqual('');
        });
        it('try it with valid parameters', () => {
            let response = service._maskReplaceSingle('http://www.test.com/[[ct]]', 'ct', '1');
            expect(response).toEqual('http://www.test.com/1');
        });
    });
});
