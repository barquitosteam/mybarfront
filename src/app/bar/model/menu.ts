import { Product } from "./product";

export interface Menu {
    id: string,
    name: string,
    products: Array<Product>,
    groups: Array<any>,
    position: number,
    active: boolean
}
