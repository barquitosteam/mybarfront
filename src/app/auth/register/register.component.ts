import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { checkPasswordValidator } from 'src/app/validators/check-password.validator';
import { AuthenticationService } from '../services/authentication.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

	loading: boolean = false;
	registerForm: FormGroup;

	constructor(
		private authenticationService: AuthenticationService,
		private router: Router,
		private snackBar: MatSnackBar,
		private formBuilder: FormBuilder) { }

	ngOnInit(): void {
		this.registerForm = this.formBuilder.group({
			cif: ['', [Validators.required]],
			password: ['', [Validators.required, Validators.minLength(6)]],
			repeated_password: ['', [Validators.required, Validators.minLength(6)]],
			name: ['', [Validators.required]],
			address: ['', [Validators.required]]
		},{
			validator: checkPasswordValidator
		});
	}

	register() {
		if (this.registerForm.valid) {
			this.loading = true;
			this.authenticationService.register(this.registerForm.value).subscribe(
				response => {
					this.loading = false;
					if (response === true) {
						let username = this.authenticationService.getCurrentUser();
						if (username && username.id) {
							this.router.navigateByUrl(this.authenticationService.redirectUrl || 'bar/' + username.id);
							this.authenticationService.redirectUrl = '';
						} else {
							this.router.navigateByUrl('login');
						}
					}
				},
				error => {
					this.loading = false;
					this.snackBar.dismiss();
					this.snackBar.open('Se produjo un error al crear el usuario', '', { duration: 5000, panelClass: ['error'] });
				}
			);
		}
	}

}
