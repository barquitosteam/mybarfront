import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { RouterModule } from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import { BarComponent } from './bar.component';
import { ManageMenusComponent } from './manage-menus/manage-menus.component';
import { SetMenuDialogComponent } from './manage-menus/set-menu-dialog/set-menu-dialog.component';
import { SetProductDialogComponent } from './manage-menus/set-product-dialog/set-product-dialog.component';
import { SetGroupDialogComponent } from './manage-menus/set-group-dialog/set-group-dialog.component';
import { ManageBarGeneralInfoComponent } from './manage-bar-general-info/manage-bar-general-info.component';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { GenerateBarQrComponent } from './generate-bar-qr/generate-bar-qr.component';
import { PublicMenusComponent } from './public-menus/public-menus.component';
import { CartComponent } from './public-menus/cart/cart.component';


@NgModule({
	declarations: [BarComponent, ManageMenusComponent, SetMenuDialogComponent, SetProductDialogComponent, SetGroupDialogComponent, ManageBarGeneralInfoComponent, GenerateBarQrComponent, PublicMenusComponent, CartComponent],
	imports: [
		CommonModule,
		MaterialModule,
		NgxQRCodeModule,
		ReactiveFormsModule,
		RouterModule
	]
})
export class BarModule { }
