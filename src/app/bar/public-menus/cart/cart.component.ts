import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../model/product';
import { CartService } from '../../services/cart.service';

@Component({
	selector: 'app-cart',
	templateUrl: './cart.component.html',
	styleUrls: ['./cart.component.sass']
})
export class CartComponent implements OnInit {

	@Input()
	barId: String = '';
	clearSelectionEnabled: boolean = false;
	cartEnabled: boolean = false;
	itemsInCart: Array<Array<Product>> = [];
	total: number = 0;

	constructor(private cartService: CartService) {}

	ngOnInit(): void {
		this.cartService.hasSelectedItems$.subscribe(response => {
			this.clearSelectionEnabled = response;
		});
		this.cartService.itemsInCart$.subscribe(response => {
			this.itemsInCart = response;
			this.total = 0;
			this.itemsInCart.forEach(item => {
				item.forEach(product => {
					this.total += (product.price * product['count']);
				});
			});
		});
		this.cartService.initializeBarCart(this.barId);
	}

	clearSelectedProducts() {
		this.cartService.clearSelectedProducts();
	}

	addSelectionToCart() {
		this.cartService.addSelectedProductsToCart();
	}

	showCart() {
		this.cartEnabled = true;
	}

	hideCart() {
		this.cartEnabled = false;
	}

	deleteCart() {
		this.cartService.deleteCart();
		this.cartEnabled = false;
	}

}
