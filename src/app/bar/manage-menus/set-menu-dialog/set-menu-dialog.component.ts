import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Menu } from '../../model/menu';

@Component({
	selector: 'app-set-menu-dialog',
	templateUrl: './set-menu-dialog.component.html',
	styleUrls: ['./set-menu-dialog.component.sass']
})
export class SetMenuDialogComponent implements OnInit {

	form: FormGroup;

	constructor(private dialogRef: MatDialogRef<any>,
		@Inject(MAT_DIALOG_DATA) public data: Menu) {
		}

	ngOnInit(): void {
		this.form = new FormGroup({
			name: new FormControl(this.data?.name || '', Validators.required)
		});
	}

	save() {
		if (this.form.valid) {
			let result = {...this.data};
			result.name = this.form.value.name;
			result.groups = result.groups || [];
			this.dialogRef.close(result);
		}
	}

}
