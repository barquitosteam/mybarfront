import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Product } from '../model/product';

@Injectable({
	providedIn: 'root'
})
export class CartService {

	private barId: String = '';
	private hasSelectedItems: BehaviorSubject<boolean> = new BehaviorSubject(false);
	hasSelectedItems$: Observable<boolean> = this.hasSelectedItems.asObservable();
	private clearSelectedItems: Subject<boolean> = new Subject();
	clearSelectedItems$: Observable<boolean> = this.clearSelectedItems.asObservable();
	private itemsInCart: BehaviorSubject<Array<Array<Product>>> = new BehaviorSubject([]);
	itemsInCart$: Observable<Array<Array<Product>>> = this.itemsInCart.asObservable();
	private products: Array<Product> = [];
	private cart: Array<Array<Product>> = [];

	constructor() {}

	initializeBarCart(barId) {
		this.barId = barId;
		this.cart = JSON.parse(sessionStorage.getItem('currentCart-' + this.barId)) || [];
		this.itemsInCart.next(this.cart);
		this.products = [];
		this.hasSelectedItems.next(false);
		this.clearSelectedItems.next(false);
	}

	addSelectedProduct(product) {
		let foundProduct = this.products.find(item => item.id === product.id);
		if (foundProduct) {
			foundProduct['count']++;
		} else {
			product['count'] = 1;
			this.products.push(JSON.parse(JSON.stringify(product)));
		}
		this.hasSelectedItems.next(true);
	}

	removeSelectedProduct(product) {
		let index = this.products.findIndex(item => item.id === product.id);
		if (index >= 0) {
			if (this.products[index]['count'] > 1) {
				this.products[index]['count']--;
			} else {
				this.products.splice(index, 1);
			}
		}
		this.hasSelectedItems.next(this.products.length > 0);
	}

	clearSelectedProducts() {
		this.products = [];
		this.hasSelectedItems.next(false);
		this.clearSelectedItems.next(true);
	}

	addSelectedProductsToCart() {
		if (this.products.length > 0) {
			this.cart.push(this.products);
			sessionStorage.setItem('currentCart-' + this.barId, JSON.stringify(this.cart));
			this.clearSelectedProducts();
			this.itemsInCart.next(this.cart);
		}
	}

	deleteCart() {
		sessionStorage.removeItem('currentCart-' + this.barId);
		this.barId = '';
		this.products = [];
		this.hasSelectedItems.next(false);
		this.cart = [];
		this.itemsInCart.next(this.cart);
	}

}
