export interface Bar {
    id: String,
    name: String,
    cif: String,
    address: String
}
