import { TestBed, getTestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { AuthGuard } from './auth.guard';
import { RouterStateSnapshot } from '@angular/router';
import createSpyObj = jasmine.createSpyObj;

describe('Service: AuthGuard', () => {
    let injector: TestBed;
    let service: AuthGuard;
    let mockSnapshot: RouterStateSnapshot;
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [],
            providers: [
                AuthGuard,
                { provide: AuthenticationService, useValue: { login: () => of(), logout: () => { }, getUserName: () => '', _redirectUrl: '/' } },
                { provide: Router, useValue: { navigate: () => { } } }
            ]
        });
        injector = getTestBed();
        service = injector.get(AuthGuard);
        mockSnapshot = createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    describe('canActivate', () => {
        it('try it with not valid currentUser in the sessionStorage', () => {
            expect(service.canActivate(null, null)).toEqual(false);
        });
        it('try it with not valid currentUser in the sessionStorage with valid state', () => {
            mockSnapshot.url = '/test';
            expect(service.canActivate(null, mockSnapshot)).toEqual(false);
        });
        it('try it with valid currentUser in the sessionStorage', () => {
            spyOn(sessionStorage, 'getItem').and.returnValue('myUser');
            expect(service.canActivate(null, null)).toEqual(true);
        });
    });
});
