import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetGroupDialogComponent } from './set-group-dialog.component';

describe('SetGroupDialogComponent', () => {
  let component: SetGroupDialogComponent;
  let fixture: ComponentFixture<SetGroupDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetGroupDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetGroupDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
