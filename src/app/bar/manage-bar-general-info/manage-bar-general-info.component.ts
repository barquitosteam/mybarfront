import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthenticationService } from 'src/app/auth/auth.module';
import { ToolbarService } from 'src/app/services/toolbar.service';
import { checkPasswordValidator } from 'src/app/validators/check-password.validator';
import { BarService } from '../services/bar.service';

@Component({
	selector: 'app-manage-bar-general-info',
	templateUrl: './manage-bar-general-info.component.html',
	styleUrls: ['./manage-bar-general-info.component.sass']
})
export class ManageBarGeneralInfoComponent implements OnInit {

	form: FormGroup;
	loading: boolean = false;
	savingContent: boolean = false;

	constructor(
		private authService: AuthenticationService, 
		private barService: BarService,
		private formBuilder: FormBuilder,
		private snackBar: MatSnackBar,
		private toolbarService: ToolbarService) {}

	ngOnInit(): void {
		let user = this.authService.getCurrentUser();
		this.toolbarService.setTitle(user.name);
		this.form = this.formBuilder.group({
			cif: ['', [Validators.required]],
			password: ['', [Validators.required, Validators.minLength(6)]],
			repeated_password: ['', [Validators.required, Validators.minLength(6)]],
			name: ['', [Validators.required]],
			address: ['', [Validators.required]]
		},{
			validator: checkPasswordValidator
		});
		this.loading = true;
		this.barService.getBarInfo(user.id).subscribe(
			response => {
				this.form.controls.cif.setValue(response?.['cif'] || '');
				this.form.controls.password.setValue('unchanged');
				this.form.controls.repeated_password.setValue('unchanged');
				this.form.controls.name.setValue(response?.['name'] || '');
				this.form.controls.address.setValue(response?.['address'] || '');
			},
			error => {

			},
			() => {
				this.loading = false;
			});
	}

	save() {
		if (this.form.valid) {
			let result = this.form.value;
			if (result.password === 'unchanged') {
				delete result.password;
				delete result.repeated_password;
			}
			this.savingContent = true;
			let user = this.authService.getCurrentUser();
			this.barService.updateBarInfo(user.id, result).subscribe(
				response => {
					if (this.form.value['name'] !== user.name) {
						this.authService.setCurrentUserField('name', this.form.value['name']);
					}
					this.snackBar.dismiss();
					this.snackBar.open('Información actualizada correctamente', '', { duration: 3000 });
					this.savingContent = false;
				},
				error => {
					this.snackBar.dismiss();
					this.snackBar.open('Se produjo un error al guardar la información', '', { duration: 5000, panelClass: ['error'] });
					this.savingContent = false;
				});
		}
	}

	checkIfDefaultValue(field, defaultString) {
		if (this.form.controls[field].value === defaultString) {
			this.form.controls[field].setValue('');
		}
	}

}
