import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UtilsService {
    replaceUrlParams(url: string = '', params) {
        for (var key in params) {
            url = this._maskReplaceSingle(url, key, params[key]);
        }
        return url || '';
    }
    _maskReplaceSingle = function (mask, key, value) {
        if (value == undefined || value === '') {
            return mask || '';
        } else {
            var regExp = new RegExp('\\[\\?{0,1}([^\\[]*)\\[' + key + '\\]([^\\]]*)\\]', 'g');
            return mask.replace(regExp, '$1' + value + '$2');
        }
    };
}

