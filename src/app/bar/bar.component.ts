import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../auth/auth.module';
import { ToolbarService } from '../services/toolbar.service';
import { Bar } from './model/bar';
import { BarService } from './services/bar.service';

@Component({
	selector: 'app-bar',
	templateUrl: './bar.component.html',
	styleUrls: ['./bar.component.sass']
})
export class BarComponent implements OnInit {

	centered = false;
	disabled = false;
	unbounded = false;
  
	radius: number;
	color: string;

	bar: Bar;
	loading: boolean = false;

	constructor(
		private authService: AuthenticationService, 
		private barService: BarService, 
		private toolbarService: ToolbarService) {}

	ngOnInit() {
		this.loading = true;
		let user = this.authService.getCurrentUser();
		this.toolbarService.setTitle(user.name);
		if (user.id) {
			this.barService.getBarInfo(user.id).subscribe(
				(response: Bar) => {
					this.bar = response;
				},
				error => {
					//set error message
				},
				() => {
					this.loading = false;	
				}
			);
		}
	}

}
