import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { delay } from 'rxjs/operators';
import { AuthenticationService } from './auth/auth.module';
import { ToolbarService } from './services/toolbar.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.sass']
})
export class AppComponent {

	title: string = '';

	constructor(
		public authService: AuthenticationService,
		private router: Router,
		public toolbarService: ToolbarService) {
			this.toolbarService.toolbarTitle$.pipe(delay(0)).subscribe(response => {
				this.title = response;
			});
		}

	redirectTo(view) {
		let user = this.authService.getCurrentUser();
		switch(view) {
			case 'manage-account':
				this.router.navigateByUrl('/bar/' + user.id + '/config');
				break;
			case 'manage-menus':
				this.router.navigateByUrl('/bar/' + user.id + '/menus');
				break;
			case 'generate-qr':
				this.router.navigateByUrl('/bar/' + user.id + '/qr');
				break;
			default:
				this.router.navigateByUrl('/bar/' + user.id);
				break;
		}
	}
}
