import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { map, catchError, timeout } from "rxjs/operators";
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { UtilsService } from 'src/app/services/utils.service';
import { ToolbarService } from 'src/app/services/toolbar.service';

@Injectable()
export class AuthenticationService {

	public token: string;
	private _redirectUrl: string;

	private loginStatus: BehaviorSubject<boolean> = new BehaviorSubject(false);
	loginStatus$: Observable<boolean> = this.loginStatus.asObservable();

	constructor(
		private http: HttpClient, 
		private router: Router,
		private toolbarService: ToolbarService,
		private utilsService: UtilsService) {
		const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
		this.token = currentUser?.token || '';
		if (currentUser) {
			this.loginStatus.next(true);
		}
	}

	get redirectUrl(): string {
		return this._redirectUrl;
	}

	set redirectUrl(url: string) {
		this._redirectUrl = url;
	}

	login(username: string, password: string) {
		if (username && password) {
			let params = {
				username: username,
				password: password
			};
			let url = this.utilsService.replaceUrlParams(environment.loginUrl, {
				baseUrl: environment.baseUrl
			});
			return this.http.post(url, params, {
				headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
				observe: 'response'
			}).pipe(timeout(10000))
				.pipe(map((response) => {
					if (response?.status == 200) {
						this.token = response.body['token'];
						sessionStorage.setItem('currentUser', JSON.stringify({ name: response.body['name'], token: this.token, id: response.body['id'] }));
						this.loginStatus.next(true);
						return true;
					} else {
						this.loginStatus.next(false);
						return false;
					}
				})).pipe(catchError(res => {
					console.warn(res);
					return throwError({
						'message': '[ERROR] - Error, there was an error in the request',
						'data': null,
						'error': true
					});
				}));
		} else {
			console.warn('[ERROR] - Error, the username and/or password are not valids');
			return throwError({
				'message': '[ERROR] - Error, the username and/or password are not valids',
				'data': null,
				'error': true
			})
		}
	}

	logout() {
		this.token = null;
		sessionStorage.removeItem('currentUser');
		this.toolbarService.setTitle('');
		this.loginStatus.next(false);
		this.router.navigate(['/login']);
	}

	getCurrentUser() {
		const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
		if (currentUser) {
			return currentUser;
		}
		this.logout();
		this.loginStatus.next(false);
		return '';
	}

	setCurrentUserField(field, value) {
		const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
		if (currentUser[field]) {
			currentUser[field] = value;
			sessionStorage.setItem('currentUser', JSON.stringify(currentUser));
			this.loginStatus.next(true);
		}
	}

	register(data) {
		let url = this.utilsService.replaceUrlParams(environment.registerUrl, {
			baseUrl: environment.baseUrl
		});
		return this.http.post(url, data, {
			headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
			observe: 'response'
		}).pipe(map(response => {
				if (response?.['status'] == 200) {
					this.token = response.body['token'];
					sessionStorage.setItem('currentUser', JSON.stringify({ name: response.body['name'], token: this.token, id: response.body['id'] }));
					this.loginStatus.next(true);
					return true;
				} else {
					this.loginStatus.next(false);
					return false;
				}
			})).pipe(catchError(res => {
				console.warn(res);
				return throwError({
					'message': '[ERROR] - Error, there was an error in the register request',
					'data': null,
					'error': true
				});
			}));
	}
}
