import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Product } from '../../model/product';

@Component({
	selector: 'app-set-product-dialog',
	templateUrl: './set-product-dialog.component.html',
	styleUrls: ['./set-product-dialog.component.sass']
})
export class SetProductDialogComponent implements OnInit {

	form: FormGroup;

	constructor(private dialogRef: MatDialogRef<any>,
				@Inject(MAT_DIALOG_DATA) public data: {product: Product, itemToEdit: string, new: boolean}) {}

	ngOnInit(): void {
		this.form = new FormGroup({
			name: new FormControl(this.data?.product?.name || '', Validators.required),
			description: new FormControl(this.data?.product?.description || ''),
			price: new FormControl(this.data?.product?.price || null, [Validators.required, Validators.min(0)]),
			newAddition: new FormControl(this.data?.product?.newAddition || false)
		});
	}

	save() {
		if (this.form.valid) {
			this.data.product['name'] = this.form.value.name;
			this.data.product['description'] = this.form.value.description;
			this.data.product['price'] = this.form.value.price;
			this.data.product['newAddition'] = this.form.value.newAddition;
			this.dialogRef.close(this.data.product);
		}
	}

	initialFocus(item) {
		if (item === this.data?.itemToEdit) {
			return this.data.itemToEdit;
		}
		return null;
	}
}
