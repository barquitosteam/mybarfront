import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import {PortalModule} from '@angular/cdk/portal';
import { MaterialModule } from 'src/app/material.module';
import { BarModule } from 'src/app/bar/bar.module';
import { AuthenticationService, AuthGuard, AuthModule } from 'src/app/auth/auth.module';

import { AppComponent } from './app.component';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		FormsModule,
		ReactiveFormsModule,
		MatNativeDateModule,
		AuthModule,
		BarModule,
		BrowserAnimationsModule,
		BrowserModule,
		AppRoutingModule,
		HttpClientModule,
		MaterialModule,
		PortalModule
	],
	providers: [AuthenticationService, AuthGuard],
	bootstrap: [AppComponent]
})
export class AppModule { }
