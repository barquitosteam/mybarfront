import { AbstractControl } from "@angular/forms";

export const checkPasswordValidator = (control: AbstractControl) => {
    if (control.get('password').value && control.get('repeated_password').value &&
        control.get('password').value != control.get('repeated_password')?.value) {
        control.get('repeated_password').setErrors({matchPassword: true});
    } else {
        control.get('repeated_password').setErrors(null);
    }
}