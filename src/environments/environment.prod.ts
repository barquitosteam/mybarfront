export const environment = {
  production: true,
  baseUrl: 'https://my-bar-app-backend.herokuapp.com',
  loginUrl: '[[baseUrl]]/login/',
  registerUrl: '[[baseUrl]]/bars/register/',
  bar: '[[baseUrl]]/bars/[[barId]]/',
  menus: '[[baseUrl]]/bars/[[barId]]/menus/',
  menu: '[[baseUrl]]/bars/[[barId]]/menus/[[menuId]]/',
  publicMenus: '[[baseUrl]]/public/bars/[[barId]]/menus/',
  qrLink: 'https://my-bar-app.herokuapp.com/bar/[[barId]]/public/menus/',
  setMenuActive: '[[baseUrl]]/bars/[[barId]]/menus/[[activeOption]]/[[menuId]]/'
};
