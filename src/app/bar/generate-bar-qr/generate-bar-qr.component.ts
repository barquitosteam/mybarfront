import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgxQrcodeElementTypes, NgxQrcodeErrorCorrectionLevels } from '@techiediaries/ngx-qrcode';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import { AuthenticationService } from 'src/app/auth/auth.module';
import { ToolbarService } from 'src/app/services/toolbar.service';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';

@Component({
	selector: 'app-generate-bar-qr',
	templateUrl: './generate-bar-qr.component.html',
	styleUrls: ['./generate-bar-qr.component.sass']
})
export class GenerateBarQrComponent implements OnInit {

	@ViewChild('screen') screen: ElementRef;
	@ViewChild('canvas') canvas: ElementRef;
	@ViewChild('downloadLink') downloadLink: ElementRef;
	elementType = NgxQrcodeElementTypes.URL;
	correctionLevel = NgxQrcodeErrorCorrectionLevels.HIGH;
	value = '';
	barName: string = '';

	constructor(
		private utilsService: UtilsService,
		private authService: AuthenticationService,
		private toolbarService: ToolbarService) { }

	ngOnInit(): void {
		let user = this.authService.getCurrentUser();
		this.toolbarService.setTitle(user.name);
		this.barName = user.name;
		this.value = this.utilsService.replaceUrlParams(environment.qrLink, {
            baseUrl: environment.baseUrl,
            barId: user.id
        });
	}

	downloadPDF() {
		html2canvas(this.screen.nativeElement).then(canvas => {
			this.canvas.nativeElement.src = canvas.toDataURL();
			//this.downloadLink.nativeElement.href = canvas.toDataURL('image/png');
			//this.downloadLink.nativeElement.download = 'QR.png';

			//let pdf = new jsPDF('l', 'cm', 'a4'); //Generates PDF in landscape mode
			let pdf = new jsPDF('p', 'cm', 'a4'); //Generates PDF in portrait mode
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 0.5, 0, 5, 5); 
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 5.5, 0, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 10.5, 0, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 15.5, 0, 5, 5); 

			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 0.5, 5, 5, 5); 
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 5.5, 5, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 10.5, 5, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 15.5, 5, 5, 5);

			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 0.5, 10, 5, 5); 
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 5.5, 10, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 10.5, 10, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 15.5, 10, 5, 5);

			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 0.5, 15, 5, 5); 
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 5.5, 15, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 10.5, 15, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 15.5, 15, 5, 5);

			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 0.5, 20, 5, 5); 
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 5.5, 20, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 10.5, 20, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 15.5, 20, 5, 5);

			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 0.5, 25, 5, 5); 
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 5.5, 25, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 10.5, 25, 5, 5);
			pdf.addImage(this.canvas.nativeElement.src, 'PNG', 15.5, 25, 5, 5);
			pdf.save('QR.pdf');
		});
	}

}
