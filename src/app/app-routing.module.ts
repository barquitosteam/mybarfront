import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BarComponent } from 'src/app/bar/bar.component';
import { ManageMenusComponent } from 'src/app/bar/manage-menus/manage-menus.component';
import { AuthGuard, LoginComponent } from './auth/auth.module';
import { RegisterComponent } from './auth/register/register.component';
import { GenerateBarQrComponent } from './bar/generate-bar-qr/generate-bar-qr.component';
import { ManageBarGeneralInfoComponent } from './bar/manage-bar-general-info/manage-bar-general-info.component';
import { PublicMenusComponent } from './bar/public-menus/public-menus.component';

const routes: Routes = [
	{ path: 'login', component: LoginComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'bar/:id', component: BarComponent, canActivate: [AuthGuard]},
	{ path: 'bar/:id/menus', component: ManageMenusComponent, canActivate: [AuthGuard]},
	{ path: 'bar/:id/public/menus', component: PublicMenusComponent},
	{ path: 'bar/:id/config', component: ManageBarGeneralInfoComponent, canActivate: [AuthGuard]},
	{ path: 'bar/:id/qr', component: GenerateBarQrComponent, canActivate: [AuthGuard]},
	{ path: '**', redirectTo: '/login' }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
