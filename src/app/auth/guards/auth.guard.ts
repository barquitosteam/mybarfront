import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { ToolbarService } from 'src/app/services/toolbar.service';
import { AuthenticationService } from '../services/authentication.service';

/** Class representing the AuthGuard.
 *  This class has a function for check if the user is logged in our application.
 *  If the user is not logged in, redirects him to the login window.
 */
@Injectable()
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router, 
        private authService: AuthenticationService,
        private toolbarService: ToolbarService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (sessionStorage.getItem('currentUser')) {
            return true;
        }
        this.toolbarService.setTitle('');
        this.authService.redirectUrl = state && state.url ? state.url : '/';
        this.router.navigate(['/login']);
        return false;
    }
}
