import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/material.module';
import { RouterModule } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { AuthenticationService } from './services/authentication.service';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
	providers: [AuthenticationService],
	declarations: [LoginComponent, RegisterComponent],
	imports: [CommonModule, FormsModule, MaterialModule, ReactiveFormsModule, RouterModule],
	exports: [LoginComponent, RegisterComponent]
})
export class AuthModule {}
export { LoginComponent, RegisterComponent, AuthGuard, AuthenticationService };
