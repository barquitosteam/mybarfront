import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ToolbarService } from 'src/app/services/toolbar.service';
import { Menu } from '../model/menu';
import { BarService } from '../services/bar.service';
import { CartService } from '../services/cart.service';

@Component({
	selector: 'app-public-menus',
	templateUrl: './public-menus.component.html',
	styleUrls: ['./public-menus.component.sass']
})
export class PublicMenusComponent implements OnInit {

	barName: String = '';
	barId: String = '';
	menus: Array<Menu> = null;
	loadingMenus: Boolean = true;

	constructor(
		private barService: BarService,
		private cartService: CartService,
		private route: ActivatedRoute,
		private toolbarService: ToolbarService) {}

	ngOnInit(): void {
		this.barId = this.route?.params?.['value']?.id;
		this.barService.getBarActiveMenus(this.barId).subscribe(
			(response: Array<Menu>) => {
				this.loadingMenus = false;
				this.menus = response['menus'];
				this.toolbarService.setTitle(response['name']);
			},
			error => {
				this.loadingMenus = false;

			});
		this.cartService.clearSelectedItems$.subscribe(response => {
			if (this.menus) {
				this.menus.forEach(menu => {
					menu.groups.forEach(group => {
						delete group.count;
						group.products.forEach(product => {
							delete product.count;
						});
					});
				});
			}
		});
	}

	addProduct(product) {
		if(product.count) {
			product.count += 1;
		} else {
			product.count = 1;
		}
		this.cartService.addSelectedProduct(product);
	}

	removeProduct(event, product) {
		event.stopPropagation();
		product.count--;
		this.cartService.removeSelectedProduct(product);
	}
}
