import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageBarGeneralInfoComponent } from './manage-bar-general-info.component';

describe('ManageBarGeneralInfoComponent', () => {
  let component: ManageBarGeneralInfoComponent;
  let fixture: ComponentFixture<ManageBarGeneralInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageBarGeneralInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageBarGeneralInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
