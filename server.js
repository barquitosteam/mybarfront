var path = require('path');
var express = require('express');
const http = require('http');
const https = require('https');
const fs = require('fs');
const app = express();

app.use(express.static(__dirname+'/dist/my-bar'));
app.all('/*', function(req, res) {
    res.sendFile(path.join(__dirname+'/dist/my-bar/index.html')); 
});

app.listen(process.env.PORT || 80);

// start https server
/*
let sslOptions = {
    key: fs.readFileSync('key.pem'),
    cert: fs.readFileSync('cert.pem')
 };
 app.use(express.static(__dirname+'/dist/my-bar'));
 app.all('/*', function(req, res) {
     console.log(req);
    res.sendFile(path.join(__dirname+'/dist/my-bar/index.html')); 
});
let serverHttps = https.createServer(sslOptions, app).listen(443);*/
//443