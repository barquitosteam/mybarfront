import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-set-group-dialog',
	templateUrl: './set-group-dialog.component.html',
	styleUrls: ['./set-group-dialog.component.sass']
})
export class SetGroupDialogComponent implements OnInit {

	form: FormGroup;

	constructor(private dialogRef: MatDialogRef<any>,
		@Inject(MAT_DIALOG_DATA) public data: any) { }

	ngOnInit(): void {
		this.form = new FormGroup({
			name: new FormControl(this.data?.name || '', Validators.required),
			price: new FormControl(this.data?.price || 0)
		});
	}

	save() {
		if (this.form.valid) {
			this.dialogRef.close(this.form.value);
		}
	}

}
