import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetProductDialogComponent } from './set-product-dialog.component';

describe('SetProductDialogComponent', () => {
  let component: SetProductDialogComponent;
  let fixture: ComponentFixture<SetProductDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetProductDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetProductDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
