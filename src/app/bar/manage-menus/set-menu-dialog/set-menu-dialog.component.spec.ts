import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetMenuDialogComponent } from './set-menu-dialog.component';

describe('SetMenuDialogComponent', () => {
  let component: SetMenuDialogComponent;
  let fixture: ComponentFixture<SetMenuDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetMenuDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetMenuDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
