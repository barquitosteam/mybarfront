import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

	loading: boolean = false;
	loginForm: FormGroup;

	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private snackBar: MatSnackBar,
		private formBuilder: FormBuilder) { }

	ngOnInit(): void {
		this.authenticationService.logout();
		this.loginForm = this.formBuilder.group({
			username: ['', [Validators.required]],
			password: ['', [Validators.required, Validators.minLength(6)]]
		});
	}

	login(): void {
		if (this.loginForm.invalid) {
			return;
		}
		this.loading = true;
		this.authenticationService.login(this.loginForm.value.username, this.loginForm.value.password)
			.subscribe(
				result => {
					if (result === true) {
						let username = this.authenticationService.getCurrentUser();
						if (username && username.id) {
							this.router.navigateByUrl(this.authenticationService.redirectUrl || 'bar/' + username.id);
							this.authenticationService.redirectUrl = '';
						} else {
							this.router.navigateByUrl('login');
						}
					} else {
						this.snackBar.dismiss();
						this.snackBar.open('Usuario y/o contraseña incorrectos', '', { duration: 30000 });
					}
					this.loading = false;
				},
				error => {
					this.snackBar.dismiss();
					this.snackBar.open('Se produjo un error al iniciar sesión', '', { duration: 5000, panelClass: ['error'] });
					this.loading = false;
				}
			);
	}
}
