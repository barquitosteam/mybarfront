import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/auth/auth.module';
import { UtilsService } from 'src/app/services/utils.service';
import { environment } from 'src/environments/environment';
import { Bar } from '../model/bar';
import { Menu } from '../model/menu';
import { Product } from '../model/product';

@Injectable({
	providedIn: 'root'
})
export class BarService {

	constructor(private authService: AuthenticationService, private http: HttpClient, private utilsService: UtilsService) {}

	getBarInfo(barId) {
        let token = this.authService.getCurrentUser().token;
		let url = this.utilsService.replaceUrlParams(environment.bar, {
            baseUrl: environment.baseUrl,
            barId: barId
        });
		return this.http.get(url, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token, 'Content-Type': 'application/json' })
        }).pipe(
            map((response: Bar) => {
                if (response) {
                    return response;
                } else {
                    return this.handleError({ 'message': `[ERROR] - Error obtaining the bar data for ${barId}` });
                }
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error obtaining the bar data for ${barId}` });
            })
        );
    }

    updateBarInfo(barId, item) {
        let token = this.authService.getCurrentUser().token;
		let url = this.utilsService.replaceUrlParams(environment.bar, {
            baseUrl: environment.baseUrl,
            barId: barId
        });
		return this.http.put(url, item, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map((response: Bar) => {
                return response;
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error updating the bar data for ${barId}` });
            })
        );
    }

    getBarMenus(barId) {
        let token = this.authService.getCurrentUser().token;
		let url = this.utilsService.replaceUrlParams(environment.menus, {
            baseUrl: environment.baseUrl,
            barId: barId
        });
		return this.http.get(url, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map((response: Array<Menu>) => {
                if (response) {
                    return response;
                } else {
                    return this.handleError({ 'message': `[ERROR] - Error obtaining the bar menus for ${barId}` });
                }
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error obtaining the bar menus for ${barId}` });
            })
        );
    }

    getBarActiveMenus(barId) {
		let url = this.utilsService.replaceUrlParams(environment.publicMenus, {
            baseUrl: environment.baseUrl,
            barId: barId
        });
		return this.http.get(url).pipe(
            map((response: Array<Menu>) => {
                if (response) {
                    return response;
                } else {
                    return this.handleError({ 'message': `[ERROR] - Error obtaining the bar public menus for ${barId}` });
                }
            })
        ).pipe(
            catchError(error => {
                return this.handleError({ 'message': `[ERROR] - Error obtaining the bar public menus for ${barId}` });
            })
        );
    }

    setBarMenusOrder(barId, menus) {
        let token = this.authService.getCurrentUser().token;
		let url = this.utilsService.replaceUrlParams(environment.menus, {
            baseUrl: environment.baseUrl,
            barId: barId
        });
        let data = this.getMenusOrder(menus);
		return this.http.put(url, data, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map((response: Array<Menu>) => {
                return response;
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error setting the bar menus order for ${barId}` });
            })
        );
    }

    addMenu(barId, item) {
        let token = this.authService.getCurrentUser().token;
        let url = this.utilsService.replaceUrlParams(environment.menus, {
            baseUrl: environment.baseUrl,
            barId: barId
        });
        this.normalizeMenu(item);
        return this.http.post(url, item, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map((response: Menu) => {
                if (response) {
                    return response;
                } else {
                    return this.handleError({ 'message': `[ERROR] - Error creating a menu for ${barId}` });
                }
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error creating a menu for ${barId}` });
            })
        );
    }

    updateMenu(barId, item) {
        let token = this.authService.getCurrentUser().token;
        let url = this.utilsService.replaceUrlParams(environment.menu, {
            baseUrl: environment.baseUrl,
            barId: barId,
            menuId: item.id
        });
        this.normalizeMenu(item);
        return this.http.put(url, item, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map((response: Product) => {
                if (response) {
                    return response;
                } else {
                    return this.handleError({ 'message': `[ERROR] - Error updating the menu ${item.id} for the bar ${barId}` });
                }
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error updating the menu ${item.id} for the bar ${barId}` });
            })
        );
    }

    deleteMenu(barId, itemId) {
        let token = this.authService.getCurrentUser().token;
        let url = this.utilsService.replaceUrlParams(environment.menu, {
            baseUrl: environment.baseUrl,
            barId: barId,
            menuId: itemId
        });
        return this.http.delete(url, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map(response => {
                return response;
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error deleting the menu ${itemId} for the bar ${barId}` });
            })
        );
    }

    setMenuActive(barId, item) {
        let token = this.authService.getCurrentUser().token;
        let url = this.utilsService.replaceUrlParams(environment.setMenuActive, {
            baseUrl: environment.baseUrl,
            barId: barId,
            activeOption: item.active ? 'active' : 'deactive',
            menuId: item.id
        });
        return this.http.put(url, item, {
            headers: new HttpHeaders({ 'Authorization': 'Bearer ' + token })
        }).pipe(
            map((response: Product) => {
                return response;
            })
        ).pipe(
            catchError(error => {
                if (error?.['status'] === 401) {
                    this.authService.logout();
                }
                return this.handleError({ 'message': `[ERROR] - Error activating the menu ${item.id} for the bar ${barId}` });
            })
        );
    }

    normalizeMenu(menu) {
        delete menu['expanded'];
        menu.groups.forEach((group, index) => {
            group.position = index;
            if (group.tmpId) {
                delete group['tmpId'];
                delete group['id'];
            }
            group.products.forEach((product, index) => {
                product.position = index;
            });
        });
    }

    getMenusOrder(menus) {
        let result = [];
        menus.forEach((menu, index) => {
            let item = {
                id: menu.id,
                position: index
            };
            result.push(item);
        });
        return result;
    }
    
	private handleError(error: any) {
        console.warn(error.message);
        return throwError({
            'message': error.message,
            'data': null,
            'error': true
        });
    }
}
