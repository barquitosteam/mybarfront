import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Menu } from '../model/menu';
import { Product } from '../model/product';
import { BarService } from '../services/bar.service';
import { SetGroupDialogComponent } from './set-group-dialog/set-group-dialog.component';
import { SetProductDialogComponent } from './set-product-dialog/set-product-dialog.component';
import { SetMenuDialogComponent } from './set-menu-dialog/set-menu-dialog.component';
import { AuthenticationService } from 'src/app/auth/auth.module';
import * as uuid from 'uuid';
import { ToolbarService } from 'src/app/services/toolbar.service';

@Component({
	selector: 'app-manage-menus',
	templateUrl: './manage-menus.component.html',
	styleUrls: ['./manage-menus.component.sass']
})
export class ManageMenusComponent implements OnInit {

	menus: Array<Menu> = [];
	loadingMenus: boolean = true;
	errorLoadingMenus: boolean = false;
	creatingMenu: boolean = false;
	dropTargetIds: object = {};

	constructor(
		private authService: AuthenticationService,
		private barService: BarService,
		private snackBar: MatSnackBar,
		private dialog: MatDialog,
		private toolbarService: ToolbarService) {}

	ngOnInit(): void {
		let user = this.authService.getCurrentUser();
		this.toolbarService.setTitle(user.name);
		if (user?.id) {
			this.barService.getBarMenus(user.id).subscribe(
				(response: Array<Menu>) => {
					this.menus = response;
					this.menus.forEach(menu => {
						this.dropTargetIds[menu.id] = [];
						menu.groups.forEach(group => {
							this.dropTargetIds[menu.id].push(group.id);
						});
					});
					this.loadingMenus = false;
					this.errorLoadingMenus = false;
				},
				error => {
					this.errorLoadingMenus = true;
					this.loadingMenus = false;
				}
			);
		}
	}

	createMenu() {
		const dialogRef = this.dialog.open(SetMenuDialogComponent, {});
		dialogRef.afterClosed().subscribe(response => {
			if (response) {
				this.creatingMenu = true;
				let user = this.authService.getCurrentUser();
				response.position = this.menus.length;
				this.barService.addMenu(user.id, response).subscribe(
					(response: Menu) => {
						this.setExpanded(response);
						this.menus.push(response);
						this.dropTargetIds[response.id] = [];
						response.groups.forEach(group => {
							this.dropTargetIds[response.id].push(group.id);
						})
						this.creatingMenu = false;
					},
					error => {
						this.creatingMenu = false;
					}
				);
			}
		});
	}

	editMenu(event, menu, index) {
		event.stopPropagation();
		const dialogRef = this.dialog.open(SetMenuDialogComponent, {
			data: menu
		});
		dialogRef.afterClosed().subscribe(response => {
			if (response) {
				this.menus[index] = response;
			}
		});
	}

	saveMenu(menu) {
		let copy = JSON.parse(JSON.stringify(menu));
		menu.updating = true;
		let user = this.authService.getCurrentUser();
		this.barService.updateMenu(user.id, copy).subscribe(
			response => {
				menu.updating = false;
				this.snackBar.dismiss();
				this.snackBar.open('Menú guardado correctamente', '', { duration: 3000 });
			},
			error => {
				menu.updating = false;
				this.snackBar.dismiss();
				this.snackBar.open('Hubo un problema al actualizar el menu', null, { 
					panelClass: ['error'],
					duration: 5000 
				});
			}
		);
	}

	deleteMenu(menu) {
		let user = this.authService.getCurrentUser();
		menu.deleting = true;
		this.barService.deleteMenu(user.id, menu.id).subscribe(
			response => {
				let index = this.menus.findIndex(item => item.id === menu.id);
				if (index >= 0) {
					this.menus.splice(index, 1);
				}
				this.snackBar.dismiss();
				this.snackBar.open('Menú borrado correctamente', '', { duration: 3000 });
			},
			error => {
				menu.deleting = false;
				this.snackBar.dismiss();
				this.snackBar.open('Hubo un problema al borrar el menu', null, { 
					panelClass: ['error'],
					duration: 5000 
				});
			});
	}

	addProduct(group) {
		const dialogRef = this.dialog.open(SetProductDialogComponent, {
			data: {
				product: {
					description: ''
				},
				new: true,
				itemToEdit: 'name'
			}
		});
		dialogRef.afterClosed().subscribe((response: Product) => {
			if (response) {
				response.available = true;
				group.products.push(response);
			}
		});
	}

	editProduct(menu, index, field) {
		const dialogRef = this.dialog.open(SetProductDialogComponent, {
			data: {
				product: menu.products[index],
				itemToEdit: field
			}
		});
		dialogRef.afterClosed().subscribe((response: Product) => {
			if (response) {
				menu.products[index] = response;
			}
		});
	}

	setAvailableProduct(menu, index, value) {
		menu.products[index].available = value;
	}

	deleteProduct(group, index) {
		group.products.splice(index, 1);
	}

	addGroup(menu) {
		const dialogRef = this.dialog.open(SetGroupDialogComponent);
		dialogRef.afterClosed().subscribe(response => {
			if (response) {
				response.products = [];
				response.tmpId = true;
				response.id = uuid.v4();
				this.dropTargetIds[menu.id].push(response.id);
				menu.groups.push(response);
			}
		});
	}

	editGroup(menu, index) {
		const dialogRef = this.dialog.open(SetGroupDialogComponent, {
			data: menu.groups[index]
		});
		dialogRef.afterClosed().subscribe(response => {
			if (response) {
				menu.groups[index].name = response.name;
				menu.groups[index].price = response.price;
			}
		});
	}

	moveGroup(menu, index, offset) {
		let item = menu.groups.splice(index, 1);
		menu.groups.splice(index + offset, 0, item[0]);
	}

	deleteGroup(menu, index) {
		menu.groups.splice(index, 1);
	}

	drop(event: CdkDragDrop<string[]>) {
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
		} else {
			delete event.previousContainer.data[event.previousIndex]['id'];
			transferArrayItem(event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex);
		}
	}

	dropMenu(event: CdkDragDrop<string[]>) {
		if (event.previousContainer === event.container && event.previousIndex !== event.currentIndex) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
			let user = this.authService.getCurrentUser();
			this.barService.setBarMenusOrder(user.id, this.menus).subscribe(
				response => {},
				error => {
					console.error(error);
				}
			);
		}
	}

	reload() {
		this.loadingMenus = true;
		this.ngOnInit();
	}

	stopPropagation(event) {
		event.stopPropagation();
	}

	setExpanded(menu) {
		this.menus.forEach(menu => {
			menu['expanded'] = false;
		})
		menu.expanded = true;
	}

	setActive(menu) {
		let user = this.authService.getCurrentUser();
		menu.active = !menu.active;
		this.barService.setMenuActive(user.id, menu).subscribe(
			response => {},
			error => {
				this.snackBar.dismiss();
				this.snackBar.open('Hubo un problema al activar menu', null, { 
					panelClass: ['error'],
					duration: 5000 
				});
				menu.active = !menu.active;
			});
	}
}
