import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateBarQrComponent } from './generate-bar-qr.component';

describe('GenerateBarQrComponent', () => {
  let component: GenerateBarQrComponent;
  let fixture: ComponentFixture<GenerateBarQrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GenerateBarQrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateBarQrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
