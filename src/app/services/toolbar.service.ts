import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
	providedIn: 'root'
})
export class ToolbarService {

	private toolbarTitle: BehaviorSubject<string> = new BehaviorSubject('');
	toolbarTitle$: Observable<string> = this.toolbarTitle.asObservable();

	constructor() { }

	setTitle(title) {
		this.toolbarTitle.next(title);
	}

}
